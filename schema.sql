-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS airport;
DROP TABLE IF EXISTS plane;
DROP TABLE IF EXISTS flight;
DROP TABLE IF EXISTS ticket;

CREATE TABLE users (
  id_user SERIAL PRIMARY KEY,
  user_name TEXT UNIQUE NOT NULL,
  pass_word TEXT NOT NULL,
  user_role INTEGER DEFAULT 0 -- 0 for user 1 for admin
);

CREATE TABLE airport (
  id_port SERIAL PRIMARY KEY,
  address TEXT NOT NULL,
  title TEXT NOT NULL
);

CREATE TABLE plane (
  id_plane SERIAL PRIMARY KEY,
  model TEXT NOT NULL,
  capacity INTEGER
);

CREATE TABLE flight (
  id_flight SERIAL PRIMARY KEY,
  plane_id INTEGER NOT NULL,
  dep_port INTEGER NOT NULL,
  arr_port INTEGER NOT NULL,
  dep_time TIMESTAMP,
  arr_time TIMESTAMP,
  empty INTEGER,
  CHECK (dep_port != arr_port),
  FOREIGN KEY (plane_id) REFERENCES plane (id_plane),
  FOREIGN KEY (dep_port) REFERENCES airport (id_port),
  FOREIGN KEY (arr_port) REFERENCES airport (id_port)
);

CREATE TABLE ticket (
  id_ticket SERIAL PRIMARY KEY,
  passenger INTEGER NOT NULL,
  flight_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  user_id TEXT NOT NULL,
  num_seats TEXT NOT NULL,
  FOREIGN KEY (flight_id) REFERENCES flight (id_flight),
  FOREIGN KEY (passenger) REFERENCES users (id_user)	
);
