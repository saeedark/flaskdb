from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from werkzeug.exceptions import abort
from werkzeug.security import generate_password_hash


from airplane.auth import login_required
from airplane.db import get_db

bp = Blueprint("tick", __name__)


@bp.route("/", methods=['GET', 'POST'])
def index():
    """Show all the posts, most recent first."""
    dep = request.form.get('departure')
    arr = request.form.get('arrival')
    print("dep", dep ," arr ", arr)
    if dep != None and arr != None and dep != arr:
        db = get_db()
        flights = db.execute(
            "SELECT f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM flight f JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " WHERE f.arr_port = ? AND f.dep_port = ?",
            (arr, dep,),
        ).fetchall()
    else:
        db = get_db()
        flights = db.execute(
            "SELECT f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM flight f JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " ORDER BY dep_time DESC"
        ).fetchall()

    airports = db.execute(
        "SELECT id_port, title"
        " FROM airport"
    ).fetchall()

    return render_template("tick/index.html", flights=flights, airports=airports)


def get_flight(id):
    flight = (
        get_db()
        .execute(
            "SELECT f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM flight f JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " WHERE f.id_flight = ?",
            (id,),
        )
        .fetchone()
    )

    if flight is None:
        abort(404, "Flight id {0} doesn't exist.".format(id))

    return flight


@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    """Create a new post for the current user."""
    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO post (title, body, author_id) VALUES (?, ?, ?)",
                (title, body, g.user["id"]),
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")


@bp.route("/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    """Update a post if the current user is the author."""
    post = get_post(id)

    if request.method == "POST":
        planeid = request.form["planeid"]
        depportid = request.form["depportid"]
        arrportid = request.form["arrportid"]       
        error = None

        if not planeid:
            error = "plane_id is required."

        if not depportid:
            error = "depportid is required."            

        if not arrportid:
            error = "arrportid is required." 

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE flight SET plane_id = ?, dep_port = ?, arr_port = ? WHERE id_flight = ?", (plane_id, dep_port, arr_port, id)
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/update.html", post=post)


@bp.route("/<int:id>/delete", methods=("POST",))
@login_required
def delete(id):
    """Delete a post.

    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    get_post(id)
    db = get_db()
    db.execute("DELETE FROM post WHERE id = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))

@bp.route("/<int:id>/updateflight", methods=("GET", "POST"))
@login_required
def updateflight(id):
    """Update a post if the current user is the author."""
    flight = get_flight(id)

    if request.method == "POST":
        planeid = request.form["planeid"]
        depportid = request.form["depportid"]
        arrportid = request.form["arrportid"]       
        error = None

        if not planeid:
            error = "plane_id is required."

        if not depportid:
            error = "depportid is required."            

        if not arrportid:
            error = "arrportid is required." 

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE flight SET plane_id = ?, dep_port = ?, arr_port = ? WHERE id_flight = ?", (planeid, depportid, arrportid, id)
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/updateflight.html", post=flight)

@bp.route("/addflight", methods=("GET", "POST"))
@login_required
def addflight():


    if request.method == "POST":
        planeid = request.form["planeid"]
        depportid = request.form["depportid"]
        arrportid = request.form["arrportid"] 
        deptime = request.form["deptime"]   
        arrtime = request.form["arrtime"]    
        seats = request.form["seats"]   
        error = None

        if not planeid:
            error = "plane_id is required."

        if not depportid:
            error = "depportid is required."            

        if not arrportid:
            error = "arrportid is required." 

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO flight (plane_id, dep_port, arr_port, dep_time, arr_time, empty) VALUES (?, ?, ?, ?, ?, ?)",
                (planeid, depportid, arrportid, deptime, arrtime, seats),
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/addflight.html")

@bp.route("/<int:id>/deleteflight", methods=("POST",))
@login_required
def deleteflight(id):
    get_flight(id)
    db = get_db()
    db.execute("DELETE FROM flight WHERE id_flight = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))

@bp.route("/<int:id>/buyflight", methods=("GET", "POST"))
@login_required
def buyflight(id):
    get_flight(id)
    db = get_db()
    db.execute("INSERT INTO ticket (passenger, flight_id) VALUES (?, ?)", (g.user["id_user"],id))
    db.commit()
    return redirect(url_for("tick.index"))

@bp.route("/tickets")
@login_required
def tickets():
    """Show all the posts, most recent first."""
    db = get_db()
    if g.user["user_role"] == 1:
        tickets = db.execute(
            "SELECT s.id_user as byrname, t.id_ticket as tkid, f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM ticket t JOIN users s on s.id_user= t.passenger JOIN flight f on t.flight_id = f.id_flight JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " ORDER BY dep_time DESC"
        ).fetchall()

    else:
        tickets = db.execute(
            "SELECT s.id_user as byrname, t.id_ticket as tkid, f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM ticket t JOIN users s on s.id_user = t.passenger JOIN flight f on t.flight_id = f.id_flight JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " WHERE t.passenger = ?",
            (g.user["id_user"],)
        ).fetchall()

    return render_template("tick/tickets.html", tickets=tickets)

def get_ticket(id):
    ticket = (
        get_db()
        .execute(
            "SELECT s.id_user as byrname, t.id_ticket as tkid, f.id_flight as flid, f.plane_id as planeid, f.dep_port as depportid, f.arr_port as arrportid, f.dep_time as dep_time, f.arr_time as arr_time, p.model as plane_name, a.title as dep_name, b.title as arr_name"
            " FROM ticket t JOIN users s on s.id_user = t.passenger JOIN flight f on t.flight_id = f.id_flight JOIN plane p ON f.plane_id = p.id_plane JOIN airport a on a.id_port = f.dep_port JOIN airport b on b.id_port = f.arr_port"
            " WHERE t.id_ticket = ?",
            (id,)
        )
        .fetchone()
    )

    if ticket is None:
        abort(404, "Ticket id {0} doesn't exist.".format(id))

    return ticket


@bp.route("/<int:id>/deleteticket", methods=("GET", "POST"))
@login_required
def deleteticket(id):
    get_ticket(id)
    db = get_db()
    db.execute("DELETE FROM ticket WHERE id_ticket = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))



@bp.route("/users")
@login_required
def users():
    db = get_db()
    users = db.execute(
        "SELECT *"
        " FROM users"
        " ORDER BY id_user DESC"
    ).fetchall()

    return render_template("tick/users.html", users=users)


@bp.route("/adduser", methods=("GET", "POST"))
@login_required
def adduser():


    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        role = request.form["user_role"]
        error = None

        if not username:
            error = "username is required."
        if not password:
            error = "password is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO users (username, password, user_role) VALUES (?, ?, ?)",
                (username, generate_password_hash(password), role),
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/adduser.html")


def get_user(id):
    user = (
        get_db()
        .execute(
            "SELECT id_user,  username, password, user_role"
            " FROM users u"
            " WHERE u.id_user = ?",
            (id,)
        )
        .fetchone()
    )

    if user is None:
        abort(404, "user id {0} doesn't exist.".format(id))

    return user

@bp.route("/<int:id>/deleteuser", methods=("GET", "POST"))
@login_required
def deleteuser(id):
    get_user(id)
    db = get_db()
    db.execute("DELETE FROM users WHERE id_user = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))




@bp.route("/planes")
@login_required
def planes():
    db = get_db()
    planes = db.execute(
        "SELECT *"
        " FROM plane"
        " ORDER BY id_plane DESC"
    ).fetchall()

    return render_template("tick/planes.html", planes=planes)


@bp.route("/addplane", methods=("GET", "POST"))
@login_required
def addplane():


    if request.method == "POST":
        model = request.form["model"]
        capacity = request.form["capacity"]
        error = None

        if not model:
            error = "model is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO plane (model, capacity) VALUES (?, ?)",
                (model, capacity),
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/addplane.html")


def get_plane(id):
    plane = (
        get_db()
        .execute(
            "SELECT *"
            " FROM plane p"
            " WHERE p.id_plane = ?",
            (id,)
        )
        .fetchone()
    )

    if plane is None:
        abort(404, "plane id {0} doesn't exist.".format(id))

    return plane

@bp.route("/<int:id>/deleteplane", methods=("GET", "POST"))
@login_required
def deleteplane(id):
    get_plane(id)
    db = get_db()
    db.execute("DELETE FROM plane WHERE id_plane = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))

@bp.route("/<int:id>/updateplane", methods=("GET", "POST"))
@login_required
def updateplane(id):
    """Update a post if the current user is the author."""
    planes = get_plane(id)

    if request.method == "POST":
        planeid = request.form["id_plane"]
        model = request.form["model"]
        capacity = request.form["capacity"]    
        error = None

        if not planeid:
            error = "plane_id is required."

        if not model:
            error = "model is required."
         

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE plane SET model = ?, capacity = ? WHERE id_plane = ?", (model, capacity, planeid)
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/updateplane.html", post=planes)

######################################################


@bp.route("/airport")
@login_required
def airports():
    db = get_db()
    airports = db.execute(
        "SELECT *"
        " FROM airport"
    ).fetchall()

    return render_template("tick/airports.html", airports=airports)


@bp.route("/addairport", methods=("GET", "POST"))
@login_required
def addairport():


    if request.method == "POST":
        address = request.form["address"]
        title = request.form["title"]
        error = None

        if not title:
            error = "title is required."
        if not address:
            error = "address is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO airport (title, address) VALUES (?, ?)",
                (title, address),
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/addairport.html")


def get_airport(id):
    airport = (
        get_db()
        .execute(
            "SELECT *"
            " FROM airport a"
            " WHERE a.id_port = ?",
            (id,)
        )
        .fetchone()
    )

    if airport is None:
        abort(404, "airport id {0} doesn't exist.".format(id))

    return airport

@bp.route("/<int:id>/deleteairport", methods=("GET", "POST"))
@login_required
def deleteairport(id):
    get_airport(id)
    db = get_db()
    db.execute("DELETE FROM airport WHERE id_port = ?", (id,))
    db.commit()
    return redirect(url_for("tick.index"))

@bp.route("/<int:id>/updateairport", methods=("GET", "POST"))
@login_required
def updateairport(id):
    """Update a post if the current user is the author."""
    airport = get_airport(id)

    if request.method == "POST":
        airportid = request.form["id_port"]
        address = request.form["address"]
        title = request.form["title"]
        error = None

        if not title:
            error = "title is required."
        if not address:
            error = "address is required."
         

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE airport SET address = ?, title = ? WHERE id_port = ?", (address, title, airportid)
            )
            db.commit()
            return redirect(url_for("tick.index"))

    return render_template("tick/updateairport.html", post=airport)


########################################################333