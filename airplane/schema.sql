-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS airport;
DROP TABLE IF EXISTS plane;
DROP TABLE IF EXISTS flight;
DROP TABLE IF EXISTS ticket;

CREATE TABLE users (
  id_user INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  user_role INTEGER DEFAULT 0 -- 0 for user 1 for admin
);

CREATE TABLE airport (
  id_port INTEGER PRIMARY KEY AUTOINCREMENT,
  address TEXT NOT NULL,
  title TEXT NOT NULL
);

CREATE TABLE plane (
  id_plane INTEGER PRIMARY KEY AUTOINCREMENT,
  model TEXT NOT NULL,
  capacity INTEGER
);

CREATE TABLE flight (
  id_flight INTEGER PRIMARY KEY AUTOINCREMENT,
  plane_id INTEGER NOT NULL,
  dep_port INTEGER NOT NULL,
  arr_port INTEGER NOT NULL,
  dep_time TIMESTAMP,
  arr_time TIMESTAMP,
  empty INTEGER,
  CHECK (dep_port != arr_port),
  FOREIGN KEY (plane_id) REFERENCES plane (id_plane),
  FOREIGN KEY (dep_port) REFERENCES airport (id_port),
  FOREIGN KEY (arr_port) REFERENCES airport (id_port)
);

CREATE TABLE ticket (
  id_ticket INTEGER PRIMARY KEY AUTOINCREMENT,
  passenger INTEGER NOT NULL,
  flight_id INTEGER NOT NULL,
  reserve_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  num_seats INTEGER,
  FOREIGN KEY (flight_id) REFERENCES flight (id_flight),
  FOREIGN KEY (passenger) REFERENCES users (id_user)	
);


-- no need of these just for ease of use --

INSERT INTO users (id_user, username, password, user_role)
VALUES (0, 'admin', 'pbkdf2:sha256:150000$g6ThZG34$bf7ba3b0bbc14800ea0659bbce75aec590d9ab49295f057d663c0f40e87b5f90' , 1);

INSERT INTO airport (id_port, address, title)
VALUES (0, 'someWhere', 'Emam');

INSERT INTO airport (address, title)
VALUES ('someWhere2', 'Kish');

INSERT INTO plane (id_plane, model)
VALUES (0, 'Boeing');

INSERT INTO flight (id_flight, plane_id, dep_port, arr_port, dep_time, arr_time)
VALUES (0, 0, 0, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);